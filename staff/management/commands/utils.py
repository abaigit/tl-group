from random import choice, randrange, randint
from staff.models import Department, Position, Employee


def create_nested_departments(levels=5):
    parents = [Department.objects.create(title=f"Department-{i}") for i in range(1, levels+1)]

    for level in range(1, levels):
        children = [Department.objects.create(title=f"Child-{parent.id}-{level}", parent=parent) for parent in parents]
        parents = children


def create_random_nested_departments(levels=5):
    parents = [Department.objects.create(title=f"Department-{i}") for i in range(1, levels+1)]

    for level in range(1, levels):
        children = []
        for i in range(levels):
            parent = choice(parents)
            children.append(Department.objects.create(title=f"Child-{parent.id}-{level}", parent=parent))
        parents = children


def create_positions(limit=25):
    positions = [Position(name=f"Position-{j}") for j in range(1, limit)]
    Position.objects.bulk_create(positions)


def create_employees(limit=50_000):
    departments_ids = Department.objects.values_list("id", flat=True)
    positions_ids = Position.objects.values_list("id", flat=True)
    employees = [Employee(name=f"Person-{i}",
                          position_id=choice(positions_ids),
                          department_id=choice(departments_ids),
                          salary=randrange(10_000, 25_000),
                          employment_date=f'{randint(2000,2023)}-{randint(1,12)}-{randint(1,28)}')
                 for i in range(limit)]
    Employee.objects.bulk_create(employees)
