from django.core.management.base import BaseCommand, CommandError
from staff.management.commands.utils import (create_nested_departments, create_random_nested_departments,
                                             create_positions, create_employees)


class Command(BaseCommand):
    help = 'Команда для запуска: python manage.py departments'

    def add_arguments(self, parser):
        parser.add_argument('structure')
        parser.add_argument("--random", action="store_true", help="Отделы в рандоме")

    def handle(self, *args, **options):
        try:
            self.stdout.write(self.style.WARNING('Creating Staff structure. . .'))
            if options["random"]:
                create_random_nested_departments()
            else:
                create_nested_departments()
            create_positions()
            create_employees()
            self.stdout.write(self.style.SUCCESS('Successfully Created Staff company'))
        except Exception as ex:
            raise CommandError('Something went wrong')


