from django.shortcuts import render
from staff.models import Department


def index(request):
    departments = Department.objects.filter(parent=None)
    return render(request, "staff/index.html", {"departments": departments})
