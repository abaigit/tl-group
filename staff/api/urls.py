from rest_framework.routers import DefaultRouter
from staff.api.views import DepartmentViewSet, EmployeeViewSet

app_name = 'staff-api'

router = DefaultRouter()
router.register(r'departments', DepartmentViewSet, basename='departments')
router.register(r'employees', EmployeeViewSet, basename='employees')
urlpatterns = router.urls
