from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from staff.models import Department, Employee
from staff.api.serializers import DepartmentSerializer, EmployeeSerializer
from staff.paginators import BasePagination


class DepartmentViewSet(ModelViewSet):
    serializer_class = DepartmentSerializer
    queryset = Department.objects.all()
    filter_backends = [SearchFilter]
    search_fields = ['title']

    def retrieve(self, request, *args, **kwargs):
        department = self.get_object()
        queryset = Department.objects.filter(parent=department)
        serializer = self.serializer_class(queryset, many=True)
        return Response(data=serializer.data)


class EmployeeViewSet(ModelViewSet):
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
    filter_backends = [SearchFilter]
    search_fields = ['name']
    pagination_class = BasePagination

    def get_queryset(self):
        department_id = self.request.query_params.get("department_id")
        if self.action == 'list' and department_id:
            self.queryset = self.queryset.filter(department_id=department_id)
        return self.queryset

