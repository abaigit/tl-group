let state = {
    departmentSubdivs: {},
    departmentEmployees: {}
}

const fetchEmployees = async (departmentId, page=1) => {
    const url = `/api/employees/?department_id=${departmentId}&page=${page}`;
    const response = await fetch(url);
    const json = await response.json();
    console.log("Employees:", json)
    if (state.departmentEmployees.hasOwnProperty(departmentId)){
        state.departmentEmployees[departmentId]["data"] = [...state.departmentEmployees[departmentId]["data"], ...json.results];
        state.departmentEmployees[departmentId]["next"] = json.next;
    } else{
        state.departmentEmployees[departmentId] = {data: json.results, next: json.next};
    }
    return json
}

const fetchSubdivisions = async (departmentId) => {
    const response = await fetch(`/api/departments/${departmentId}/`);
    const json = await response.json();
    console.log("Departments:", json);
    state.departmentSubdivs[departmentId] = json
    return json;
}

const renderEmployees = (departmentId) => {
    const departmentEmployees = state.departmentEmployees[departmentId];
    const employeeListDiv = $("#employees-list");
    employeeListDiv.empty();
    $(departmentEmployees.data).each((index, employee) => {
        const employeeRow = `
            <div class="row">
                <div class="col-4">${employee.name}</div>
                <div class="col-3">${employee.position.name}</div>
                <div class="col-3">${employee.salary}</div>
                <div class="col-2">${employee.employment_date}</div>
            </div>`
        employeeListDiv.append(employeeRow);
    });
    if (departmentEmployees.next){
        employeeListDiv.append(`<button class="btn btn-primary btn-sm my-4 load-more w-100" 
                                        data-department-id="${departmentId}"
                                        data-next="${departmentEmployees.next}">Загрузить еще</button>`);
    }
}

const loadMore = async (departmentId, nextPage) => {
    await fetchEmployees(departmentId, nextPage);
    renderEmployees(departmentId);
}

const renderSubdivisions = (subdivisions, department) => {
    const subdivisionsBtnArray = $(subdivisions).map((index, item) => {
        return `<li class="list-group-item text-start border-0 p-0 ps-3" id="${item.id}">
                    <span class="nav-btn p-3 d-block border border-2 border-info-subtle rounded">${item.title}</span>
                </li>`
    });
    if (subdivisionsBtnArray.length > 0){
        $(department).append(
            `<ul class="list-group rounded-0 collapse">
                ${subdivisionsBtnArray.get().join("")}
            </ul>`
        );
    }
}

$(document).ready(() => {
    $('.list-group-item').on('click', async (e) => {
        const target = e.target;
        const department = target.closest("li");
        const departmentId = +$(department).attr("id");
        $(".nav-btn").removeClass("bg-primary text-light");
        $(target).closest(".nav-btn").toggleClass("bg-primary text-light");
        if (!state.departmentSubdivs.hasOwnProperty(departmentId)){
            await fetchEmployees(departmentId);
            await fetchSubdivisions(departmentId);
            renderSubdivisions(state.departmentSubdivs[departmentId], department);
        }
        $(department).find("ul").toggleClass("show");
        renderEmployees(departmentId);
    });

    $(document).on('click', '.load-more', async function() {
        const departmentId = $(this).data('department-id');
        const nextPage = $(this).data('next');
        await loadMore(departmentId, nextPage);
    });
});
