from django.urls import path, include
from staff.views import index

app_name = 'staff'

urlpatterns = [
    path("", index, name='index'),
]
