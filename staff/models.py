from django.db import models


class Department(models.Model):
    title = models.CharField(verbose_name="Наименование отдела", max_length=255)
    parent = models.ForeignKey('self', verbose_name="Родитель", on_delete=models.CASCADE, null=True, blank=True, related_name="subdivisions")

    class Meta:
        verbose_name = "Отдел"
        verbose_name_plural = "Отделы"

    def __str__(self):
        return self.title


class Position(models.Model):
    """Позиция (должность) на какую либо работу создается до найма сотрудника на неё"""

    name = models.CharField(verbose_name="Наименование должности", max_length=255)

    class Meta:
        verbose_name = "Должность"
        verbose_name_plural = "Должности"

    def __str__(self):
        return self.name


class Employee(models.Model):
    """При приеме сотрудника выбирается его должность и отдел в котором будет работать.
    Перед реформированием отдела необходимо перевести сотрудника в другой отдел"""

    name = models.CharField(verbose_name="Ф.И.О сотрудника", max_length=255)
    position = models.ForeignKey(Position, verbose_name="Должность", on_delete=models.RESTRICT, related_name="employees")
    employment_date = models.DateField(verbose_name="Дата приема на работу")
    salary = models.DecimalField(verbose_name="Зарплата", max_digits=10, decimal_places=2)
    department = models.ForeignKey(Department, verbose_name="Отдел", on_delete=models.RESTRICT, related_name="employees")

    class Meta:
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"

    def __str__(self):
        return self.name
